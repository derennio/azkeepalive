﻿using System;
using System.IO;
using System.Net;
using System.Timers;

namespace AzureAppServiceKeeper
{
    internal class Program
    {
        private static Timer aTimer;

        private static string[] urls = new string[] { "https://arena7service.azurewebsites.net/api/DeliveryRequirement", "https://arena7fe.azurewebsites.net",
            "https://arena7adminfe.azurewebsites.net", "https://arena3service.azurewebsites.net/api/DeliveryRequirement", "https://arena3fe.azurewebsites.net",
            "https://arena3adminfe.azurewebsites.net" };

        public static void Main()
        {
            // Create a timer and set a two minute interval.
            aTimer = new Timer();
            aTimer.Interval = 120000;

            // Hook up the Elapsed event for the timer.
            aTimer.Elapsed += OnTimedEvent;

            // Have the timer fire repeated events (true is the default)
            aTimer.AutoReset = true;

            // Start the timer
            aTimer.Enabled = true;

            Console.WriteLine("Press the Enter key to exit the program at any time... ");
            Console.ReadLine();
        }

        private static void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            SendKeepAliveRequest();
        }

        private static void SendKeepAliveRequest()
        {
            foreach (var x in urls)
            {
                string url = @x;

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.AutomaticDecompression = DecompressionMethods.GZip;

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    reader.ReadToEnd();
                }

                Console.WriteLine($"Successfully pinged {url}");
            }
        }
    }
}